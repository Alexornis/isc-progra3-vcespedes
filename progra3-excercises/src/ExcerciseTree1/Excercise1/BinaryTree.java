package ExcerciseTree1.Excercise1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

public class BinaryTree<T extends Comparable<T>> implements IBinaryTree<T> {
    private INode<T> root;

    public BinaryTree() {
        this.root = null;
    }

    public String traverseIterativeBFS(){
        ArrayList<INode<T>> finalList =  new ArrayList<>();
        ArrayList<INode<T>> currentLevelNodes =  new ArrayList<>();
        ArrayList<INode<T>> nextLevelNodes = new ArrayList<>();
        if(root != null){
            nextLevelNodes.add(root);
            while (!nextLevelNodes.isEmpty()){
                finalList.addAll(nextLevelNodes);
                currentLevelNodes.addAll(nextLevelNodes);
                nextLevelNodes.clear();
                for (INode<T> node : currentLevelNodes){
                    if (node.hasLeft()) nextLevelNodes.add(node.getLeftNode());
                    if (node.hasRight()) nextLevelNodes.add(node.getRightNode());
                }
                currentLevelNodes.clear();
            }
        }
        return toStr(finalList);
    }

    private String toStr(ArrayList<INode<T>> nodes){
        StringBuilder str = new StringBuilder();
        for (INode<T> node : nodes){
            str.append(" - ").append(node.getData());
        }
        return str.toString();
    }

    public boolean isBinarySearchTree(){
        if(root == null) return true;
        else {
            ArrayList<INode<T>> currentLevelNodes =  new ArrayList<>();
            ArrayList<INode<T>> nextLevelNodes = new ArrayList<>();
            nextLevelNodes.add(root);
            while (!nextLevelNodes.isEmpty()){
                currentLevelNodes.addAll(nextLevelNodes);
                nextLevelNodes.clear();
                for (INode<T> node : currentLevelNodes){
                    if (node.hasLeft()) {
                        if(node.getData().compareTo(node.getLeftNode().getData()) < 0) return false;
                        else nextLevelNodes.add(node.getLeftNode());
                    }
                    if (node.hasRight()) {
                        if(node.getData().compareTo(node.getRightNode().getData()) > 0) return false;
                        else nextLevelNodes.add(node.getRightNode());
                    }
                }
                currentLevelNodes.clear();
            }

        }
        return  true;
    }

    @Override
    public void print() {

    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public boolean insert(T newData) {
        if (root == null) {
            root = new Node<>(newData);
            return true;//40
        }
        ArrayList<INode<T>> childs = new ArrayList<>();
        childs.add(root);
        return insertRecursive(childs, newData);

    }

    private boolean insertRecursive(ArrayList<INode<T>> nodes, T newData) {

        for(INode<T> node : nodes){
            if (!node.hasLeft()) {
                node.setLeftNode(new Node<>(newData));
                return true;
            } else if(!node.hasRight()){
                node.setRightNode(new Node<>(newData));
                return true;
            }
        }

        ArrayList<INode<T>> nextNodes = new ArrayList<>();
        for(INode<T> node : nodes){
            nextNodes.add(node.getLeftNode());
            nextNodes.add(node.getRightNode());
        }
        return insertRecursive(nextNodes,newData);
    }

    //Traversal Tree
    public void printPreOrderRecursiveDFS() {
        printPreOrderRecursiveDFS(root);
    }


    @Override
    public void printInOrderRecursiveDFS() {
        printInOrderRecursiveDFS(root);
    }

    public void printInOrderRecursiveDFS(INode<T> node) {
        if (node != null) {
            printInOrderRecursiveDFS(node.getLeftNode());
            System.out.print(node.getData() + " - ");
            printInOrderRecursiveDFS(node.getRightNode());
        }
    }

    @Override
    public void printInOrderIterativeDFS() {
        Stack<INode<T>> stack =  new Stack<>();
        INode<T> currentNode = this.root;

        while (currentNode != null || !stack.isEmpty()) {
            while (currentNode != null) {
                stack.push(currentNode);
                currentNode = currentNode.getLeftNode();
            }

            INode<T> n = stack.pop();
            System.out.print(n.getData() + " - ");
            currentNode = n.getRightNode();
        }
    }

    @Override
    public Iterator<INode<T>> iterator() {
        return null;
    }

    @Override
    public int getNumberOfNodes() {
        return 0;
    }

    @Override
    public int height() {
        return 0;
    }

    @Override
    public void delete(T node) {

    }

    private void printPreOrderRecursiveDFS(INode<T> root) {
        if (root == null) {
            return;
        }
        System.out.print(root.getData() + " ");
        printPreOrderRecursiveDFS(root.getLeftNode());
        printPreOrderRecursiveDFS(root.getRightNode());
    }

    @Override
    public void printBFS() {

    }

    @Override
    public void printDFS() {

    }

    @Override
    public void printPreOrderIterativeDFS() {

    }
}