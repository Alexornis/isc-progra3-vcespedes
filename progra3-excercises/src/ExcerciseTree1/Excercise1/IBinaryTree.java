package ExcerciseTree1.Excercise1;

import java.util.Iterator;

public interface IBinaryTree<T> {
    boolean insert(T node);
    void delete(T node);

    void print();

    int getWeight();
    int getHeight();

    void printBFS(); // preorder o postorder
    void printDFS(); // preorder o postorder

    void printPreOrderIterativeDFS();
    void printPreOrderRecursiveDFS();

    void printInOrderRecursiveDFS();
    void printInOrderIterativeDFS();

    Iterator<INode<T>> iterator();

    int getNumberOfNodes();
    int height();
}
