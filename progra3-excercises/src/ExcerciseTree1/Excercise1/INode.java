package ExcerciseTree1.Excercise1;

public interface INode<T> {
    T getData();
    void setData(T d);

    INode<T> getLeftNode();
    void setLeftNode(INode<T> leftNode);

    INode<T> getRightNode();
    void setRightNode(INode<T> rightNode);

    int getLevel();

    boolean hasLeft();

    boolean hasRight();
}
