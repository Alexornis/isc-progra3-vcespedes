package ExcerciseTree1.Excercise1;

public class Main {
    public static void main(String[] args) {

        BinaryTree<Character> binaryTree = new BinaryTree<>();

        binaryTree.insert('E');
        binaryTree.insert('C');
        binaryTree.insert('D');
        binaryTree.insert('A');
        binaryTree.insert('B');
        binaryTree.insert('F');
        binaryTree.insert('G');

        System.out.println(binaryTree.traverseIterativeBFS());

        System.out.println("this tree is a binary search tree?");
        System.out.println(binaryTree.isBinarySearchTree());
    }
}
