package ExcerciseTree1.Excercise2;

public class Main {
    public static void main(String[] args) {
        TreeManager<Integer> manager = new TreeManager<>();

        BinaryTree<Integer> case1Node1 = new BinaryTree<>();
        case1Node1.insert(1,2);
        case1Node1.insert(2,1);
        case1Node1.insert(3,3);

        BinaryTree<Integer> case1Node2 = new BinaryTree<>();
        case1Node2.insert(1,2);
        case1Node2.insert(2,1);
        case1Node2.insert(3,3);

        System.out.println("Case 1");
        System.out.println(" tree A is equals to tree B :");
        System.out.println(manager.compareTo(case1Node1,case1Node2) + "\n");

        BinaryTree<Integer> case2Node1 = new BinaryTree<>();
        case2Node1.insert(1,2);
        case2Node1.insert(3,1);
        case2Node1.insert(3,3);

        BinaryTree<Integer> case2Node2 = new BinaryTree<>();
        case2Node2.insert(1,2);
        case2Node2.insert(2,1);
        case2Node2.insert(3,3);

        System.out.println("Case 2");
        System.out.println(" tree A is equals to tree B :");
        System.out.println(manager.compareTo(case2Node1,case2Node2) + "\n");


        BinaryTree<Integer> case3Node1 = new BinaryTree<>();
        case3Node1.insert(1,2);
        case3Node1.insert(2,1);
        case3Node1.insert(3,3);

        BinaryTree<Integer> case3Node2 = new BinaryTree<>();
        case3Node2.insert(1,3);
        case3Node2.insert(2,2);
        case3Node2.insert(3,1);

        System.out.println("Case 3");
        System.out.println(" tree A is equals to tree B :");
        System.out.println(manager.compareTo(case3Node1,case3Node2) + "\n");

    }
}
