package ExcerciseTree1.Excercise2;

import java.util.ArrayList;

public class TreeManager<T extends Comparable<T>> {
    public TreeManager(){

    }
    public boolean compareTo(BinaryTree<T> treeA, BinaryTree<T> treeB){
        ArrayList<INode<T>> currentLevelNodes1 =  new ArrayList<>(), currentLevelNodes2 =  new ArrayList<>();
        ArrayList<INode<T>> nextLevelNodes1 = new ArrayList<>(), nextLevelNodes2 = new ArrayList<>();
        if(treeA.getNumberOfNodes() != treeB.getNumberOfNodes()) return false;

        else{
            if(!treeA.getRoot().equalsNode(treeB.getRoot())) return false;
            nextLevelNodes1.add(treeA.getRoot());nextLevelNodes2.add(treeB.getRoot());

            while (!nextLevelNodes1.isEmpty()){

                currentLevelNodes1.addAll(nextLevelNodes1);currentLevelNodes2.addAll(nextLevelNodes2);
                nextLevelNodes1.clear();nextLevelNodes2.clear();

                for (int i = 0; i < currentLevelNodes1.size(); i++) {
                    if (currentLevelNodes1.get(i).equalsNode(currentLevelNodes2.get(i))){
                        if (currentLevelNodes1.get(i).hasLeft()) {
                            nextLevelNodes1.add(currentLevelNodes1.get(i).getLeftNode());
                            nextLevelNodes2.add(currentLevelNodes2.get(i).getLeftNode());
                        }

                        if (currentLevelNodes1.get(i).hasRight()) {
                            nextLevelNodes1.add(currentLevelNodes1.get(i).getRightNode());
                            nextLevelNodes2.add(currentLevelNodes2.get(i).getRightNode());
                        }
                    }
                    else return false;
                }
                currentLevelNodes1.clear();currentLevelNodes2.clear();
            }
        }
        return true;
    }
}
