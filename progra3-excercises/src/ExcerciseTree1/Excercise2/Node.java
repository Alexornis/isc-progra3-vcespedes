package ExcerciseTree1.Excercise2;

public class Node<T> implements INode<T> {
    //instance variable of Node class
    private T data;
    private INode<T> left;
    private INode<T> right;
    private int key;

    //constructor
    public Node(T data, int key) {
        this.data = data;
        this.left = null;
        this.right = null;
        this.key = key;
    }

    public T getData() {
        return data;
    }

    @Override
    public void setData(T d) {
        this.data = d;
    }

    @Override
    public INode<T> getLeftNode() {
        return this.left;
    }

    @Override
    public INode<T> getRightNode() {
        return this.right;
    }

    @Override
    public int getLevel() {
        return 0;
    }

    public void setLeftNode(INode<T> left) {
        this.left = left;
    }

    public void setRightNode(INode<T> right) {
        this.right = right;
    }

    @Override
    public boolean hasLeft(){
        return left != null;
    }

    @Override
    public boolean hasRight(){
        return right != null;
    }

    @Override
    public int getKey() {
        return key;
    }

    @Override
    public boolean equalsNode(INode<T> node){
        if (!(data == node.getData()) || key != node.getKey()) return false;
        if (hasLeft() != node.hasLeft())  return false;
        if (hasRight() != node.hasRight()) return false;
        return true;
    }
}