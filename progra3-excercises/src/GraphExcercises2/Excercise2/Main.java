package GraphExcercises2.Excercise2;

public class Main {

    public static void main(String[] args) {

        WeightedGraph<Integer> graph = new WeightedGraph<>(
                new Vertex<>(1),new Vertex<>(2),new Vertex<>(3),
                new Vertex<>(4),new Vertex<>(5),new Vertex<>(6),
                new Vertex<>(7),new Vertex<>(8));

        graph.makeUnDirectionalEdge(1,3,0.23);
        graph.makeUnDirectionalEdge(1,2,0.35);
        graph.makeUnDirectionalEdge(1,4,0.23);
        graph.makeUnDirectionalEdge(2,3,0.24);
        graph.makeUnDirectionalEdge(2,4,0.74);
        graph.makeUnDirectionalEdge(2,6,0.26);
        graph.makeUnDirectionalEdge(3,5,0.51);
        graph.makeUnDirectionalEdge(3,7,0.14);
        graph.makeUnDirectionalEdge(4,6,0.24);
        graph.makeUnDirectionalEdge(5,7,0.15);
        graph.makeUnDirectionalEdge(6,8,0.32);
        graph.makeUnDirectionalEdge(7,8,0.32);

        GraphManager<Integer> manager = new GraphManager<>(graph);

        manager.searchRoutes(1,8);
        manager.showRoutes();

    }
}
