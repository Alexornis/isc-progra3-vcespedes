package GraphExcercises2.Excercise2;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class GraphManager<T> {
    private ArrayList<T> visited;
    private WeightedGraph<T> weightedGraph;
    private ArrayList<ArrayList<T>> routes;
    private ArrayList<ArrayList<Integer>> routesValues;
    private T sourceNode;
    private T searchNode;

    public GraphManager(WeightedGraph<T> weightedGraph){
        this.visited = new ArrayList<>();
        this.weightedGraph = weightedGraph;
    }

    public void searchRoutes(T sourceSearch, T finishSearch){
        this.routes = new ArrayList<>();
        this.routesValues = new ArrayList<>();
        this.sourceNode = sourceSearch;
        this.searchNode = finishSearch;
        deepFirsSearch(weightedGraph.getVertex(sourceSearch),finishSearch);
    }

    public void deepFirsSearch(Vertex<T> sourceVertex, T finishSearch){
        visited.add(sourceVertex.getValue());

        for (Vertex<T> vertex : sourceVertex.getEdges()){
            if(!visited.contains(vertex.getValue())){
                if(vertex.getValue().equals(finishSearch)){
                    ArrayList<T> route = new ArrayList<>(visited);
                    route.add(finishSearch);
                    routes.add(route);
                }else deepFirsSearch(vertex,finishSearch);
            }
        }
        visited.remove(sourceVertex.getValue());
    }

    public void showRoutes(){
        System.out.println("the possible routes from " + sourceNode + " to point " + searchNode + "  are :");
        if (routes.isEmpty()) System.out.println("there are no possible routes");
        else {
            for (ArrayList<T> vertexList : routes){
                String route = "";
                for (T vertex : vertexList){
                    route = route + "-->" + vertex;
                }
                System.out.println(route + calculateTotalWeigh(vertexList) );
            }
        }
    }


    public String calculateTotalWeigh(ArrayList<T> vertexList){
        double routeValue = 0 ;
        for (int i = 0; i < vertexList.size() - 1; i++) {
            Vertex<T> currentVertex = weightedGraph.getVertex(vertexList.get(i));
            Vertex<T> nextVertex = weightedGraph.getVertex(vertexList.get(i + 1));
            routeValue = routeValue + currentVertex.getValueEdge(nextVertex.getValue());
        }
        DecimalFormat format = new DecimalFormat("#.00");
        return " : TOTAL WEIGH : " + format.format(routeValue) ;
    }
}
