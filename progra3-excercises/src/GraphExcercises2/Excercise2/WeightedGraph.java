package GraphExcercises2.Excercise2;

import java.util.ArrayList;

public class WeightedGraph<T> {
    private Vertex<T>[] vertices;
    public WeightedGraph(Vertex<T>... vertices){
        this.vertices = vertices;
    }

    public void makeUnDirectionalEdge(T value1,T value2, double valueEdge){
        Vertex<T> firstUser = getVertex(value1);
        Vertex<T> secondUser = getVertex(value2);

        firstUser.addRelationship(secondUser,valueEdge);
        secondUser.addRelationship(firstUser,valueEdge);
    }

    public void makeDirectionalEdge(T value1,T value2, double valueEdge){
        Vertex<T> firstUser = getVertex(value1);
        Vertex<T> secondUser = getVertex(value2);

        firstUser.addRelationship(secondUser,valueEdge);
    }

    public Vertex<T> getVertex(T value){
        Vertex<T> v = null;
        for (Vertex<T> vertex : vertices){
            if (vertex.getValue().equals(value)) v = vertex;
        }
        return v;
    }

    @Override
    public String toString(){
        String message = "";
        for (Vertex<T> vertex : vertices){
            message = message + " -" + vertex.getValue() + vertex + "\n";
        }
        return message;
    }

    public Vertex<T>[] getVertices() {
        return vertices;
    }
}
