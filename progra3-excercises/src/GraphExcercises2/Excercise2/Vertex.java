package GraphExcercises2.Excercise2;


import java.util.ArrayList;

public class Vertex<T> {
    private T value ;
    private ArrayList<Vertex<T>> edges;
    private ArrayList<Double> valueEdges;

    public Vertex(T value){
        this.value = value;
        this.edges = new ArrayList<>();
        this.valueEdges = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o){
        if(!(o instanceof Vertex)) return false;
        Vertex<T> nod = (Vertex<T>) o;
        if(nod.getValue() != this.value ) return false;
        else {
            ArrayList<Vertex<T>> friendsListObject = nod.getEdges();
            if(!compareFriendsList(friendsListObject)) return false;
        }
        return true;
    }

    private boolean compareFriendsList(ArrayList<Vertex<T>> list){
        if(edges.size() != list.size()) return false;

        for (Vertex<T> connection : edges) {
            if (!listContainsVertex(connection,list)) return false;
        }

        return true;
    }

    private boolean listContainsVertex(Vertex<T> ver, ArrayList<Vertex<T>> list){
        if(list != null){
            for (Vertex<T> vertex2 : list){
                if(ver.getValue().equals(vertex2.getValue())) return true;
            }
        }
        return false;
    }

    public T getValue() {
        return value;
    }

    public ArrayList<Vertex<T>> getEdges() {
        return edges;
    }

    public void addRelationship(Vertex<T> vertex, double valueEdge ){
        if(!relationshipAlreadyExists(vertex)){
            edges.add(vertex);
            valueEdges.add(valueEdge);
        }
    }

    private boolean relationshipAlreadyExists(Vertex<T> vertex){
        for (Vertex<T> connection : edges) {
            if (connection.getValue().equals(vertex.getValue())) return true;
        }
        return false;
    }

    @Override
    public String toString(){
        String message = "" ;
        for (Vertex<T> vertex : edges){
            message = message + "     ";
            message = message + value + " --- "+ vertex.getValue();
        }
        return message;
    }

    public double getValueEdge(T value) {
        for (int i = 0; i < edges.size(); i++) {
            if (edges.get(i).getValue().equals(value)) return valueEdges.get(i);
        }
        return 0;
    }

}
