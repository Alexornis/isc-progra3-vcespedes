package GraphExcercises2.Excercise1;

public class Main {

    public static void main(String[] args){
        Graph<String> graph = new Graph(
                new Vertex<>("Algebra"),new Vertex<>("Geometry"),new Vertex<>("Statistics"),
                new Vertex<>("Trigonometry"),new Vertex<>("Calculus"),new Vertex<>("Machine Learning"));

        graph.makeDirectionalEdge("Algebra","Statistics");
        graph.makeDirectionalEdge("Algebra","Geometry");
        graph.makeDirectionalEdge("Geometry","Trigonometry");
        graph.makeDirectionalEdge("Trigonometry","Calculus");
        graph.makeDirectionalEdge("Calculus","Machine Learning");
        graph.makeDirectionalEdge("Statistics","Machine Learning");


        GraphManager<String> manager = new GraphManager<>(graph);

        System.out.println(manager.searchRequirements("Machine Learning"));
        System.out.println(manager.searchRequirements("Statistics"));

    }
}
