package GraphExcercises2.Excercise1;

import java.util.ArrayList;

public class GraphManager<T> {
    private Graph<T> graph;

    public GraphManager(Graph<T> graph){
        this.graph = graph;
    }

    public String searchRequirements(T valueWanted){
        ArrayList<Vertex<T>> listRequirements = new ArrayList<>();
        Vertex<T>[] vertices = graph.getVertices();
        for (Vertex<T> ver : vertices){
            if (!ver.getValue().equals(valueWanted) ){
                if (ver.getListConnections().contains(valueWanted))
                    listRequirements.add(ver);
            }
        }
        return showRequirements(listRequirements,valueWanted);
    }

    private String showRequirements(ArrayList<Vertex<T>> list, T valueWanted){
        StringBuilder message = new StringBuilder(" Requirements for " + valueWanted + ": \n");
        for(Vertex<T> vertex : list){
            message.append(" - ").append(vertex.getValue()).append("\n");
        }
        return message.toString();
    }
}
