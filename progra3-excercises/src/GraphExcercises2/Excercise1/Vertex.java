package GraphExcercises2.Excercise1;

import java.util.ArrayList;

public class Vertex<T> {
    private T value ;
    private ArrayList<Vertex<T>> connections;

    public Vertex(T value){
        this.value = value;
        this.connections = new ArrayList<>();
    }

    @Override
    public boolean equals(Object o){
        if(!(o instanceof Vertex)) return false;
        Vertex<T> nod = (Vertex<T>) o;
        if(nod.getValue() != this.value ) return false;
        else {
            ArrayList<Vertex<T>> friendsListObject = nod.getConnections();
            if(!compareFriendsList(friendsListObject)) return false;
        }
        return true;
    }

    private boolean compareFriendsList(ArrayList<Vertex<T>> list){
        if(connections.size() != list.size()) return false;

        for (Vertex<T> connection : connections) {
            if (!listContainsVertex(connection,list)) return false;
        }

        return true;
    }

    private boolean listContainsVertex(Vertex<T> ver, ArrayList<Vertex<T>> list){
        if(list != null){
            for (Vertex<T> vertex2 : list){
                if(ver.getValue().equals(vertex2.getValue())) return true;
            }
        }
        return false;
    }

    public T getValue() {
        return value;
    }

    public ArrayList<Vertex<T>> getConnections() {
        return connections;
    }

    public ArrayList<T> getListConnections(){
        ArrayList<T> listConnections = new ArrayList<>();
        for (Vertex<T> vertex : connections){
            listConnections.add(vertex.getValue());
        }
        return listConnections;
    }

    public void addRelationship(Vertex<T> vertex){
        if(!relationshipAlreadyExists(vertex)){
            connections.add(vertex);
        }
    }

    private boolean relationshipAlreadyExists(Vertex<T> vertex){
        for (Vertex<T> connection : connections) {
            if (connection.getValue().equals(vertex.getValue())) return true;
        }
        return false;
    }

    @Override
    public String toString(){
        String message = "" ;
        for (Vertex<T> vertex : connections){
            message = message + "     ";
            message = message + value + " --- "+ vertex.getValue();
        }
        return message;
    }
}
