package exercises.exercise3;

import java.util.Arrays;

public class Sequential {
    private final int[] sequence;

    public Sequential(int... numbers){
        this.sequence = numbers;
    }

    public int[] getSequence() {
        return sequence;
    }

    public int length(){
        return sequence.length;
    }

    @Override
    public String toString(){
        return Arrays.toString(sequence);
    }
}
