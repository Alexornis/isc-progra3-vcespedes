package exercises.exercise3;

public class Main {
    public static void main(String[] args){
        SearchList searchList = new SearchList(7,4,9,2,15,3,4,8,1,8,5,2,15,11,13,2,15);
        Sequential sequential = new Sequential(2, 15);

        System.out.println(searchList);
        System.out.println(sequential);
        System.out.println(searchList.search(sequential));
    }
}
