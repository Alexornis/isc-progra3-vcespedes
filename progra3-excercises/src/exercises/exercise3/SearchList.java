package exercises.exercise3;

import java.util.ArrayList;
import java.util.Arrays;

public class SearchList {
    private final int[] list;

    public SearchList(int... numbers){
        this.list = numbers;
    }

    public ArrayList<Integer> search(Sequential sequential){
        ArrayList<Integer> countIndex = new ArrayList<>();
        int[] sequence = sequential.getSequence();

        for (int i = 0; i < list.length - sequential.length() + 1; i++) {
            if(list[i] == sequence[0]){
                boolean aux = true;
                for (int j = 0; j < sequential.length(); j++) {
                    aux = compareNumbers(list[i + j],sequence[j]);
                }
                if(aux) countIndex.add(i);
            }
        }
        return countIndex;
    }

    private boolean compareNumbers(int num1, int num2){
        return num1 == num2;
    }

    @Override
    public String toString(){
        return Arrays.toString(list);
    }
}
