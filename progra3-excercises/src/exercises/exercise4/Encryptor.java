package exercises.exercise4;

import java.util.ArrayList;

public class Encryptor {

    public static void encrypt(Message message){
        ArrayList<Word> words = message.getWords();
        for (Word word : words) {
            changeChar(word, word.length() - 1);
            encryptFirsChar(word);
        }
    }

    private static void encryptFirsChar(Word word){
        char[] chars = word.getCharList();
        int asciiCode = (int) chars[0];
        StringBuilder newWord = new StringBuilder(asciiCode + "");
        for (int i = 1; i < chars.length; i++) {
            newWord.append(chars[i]);
        }
        word.setWord(newWord.toString());
    }

    private static void changeChar(Word word, int index2){
        char[] chars = word.getCharList();
        char char1 = chars[1];
        char char2 = chars[index2];

        chars[1] = char2;
        chars[index2] = char1;

        StringBuilder newWord = new StringBuilder();
        for (char aChar : chars) {
            newWord.append(aChar);
        }

        word.setWord(newWord.toString());
    }
}
