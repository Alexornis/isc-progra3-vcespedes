package exercises.exercise4;

public class Word {
    private String word;

    public Word(String word){
        this.word = word;
    }

    public char[] getCharList(){
        char[] charList = new char[word.length()];
        for (int i = 0; i < word.length(); i++) {
            charList[i] = word.charAt(i);
        }
        return charList;
    }

    public int length(){
        return word.length();
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
