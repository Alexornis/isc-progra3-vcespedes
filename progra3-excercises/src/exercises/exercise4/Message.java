package exercises.exercise4;

import java.util.ArrayList;

public class Message {
    private String message;
    private ArrayList<Word> words;

    public Message(String message){
        this.message = message;
        this.words = new ArrayList<>();
        divideWords();
    }

    public void divideWords(){
        String[] parts = message.split(" ");
        for (String part : parts) {
            words.add(new Word(part));
        }
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<Word> getWords() {
        return words;
    }

    @Override
    public String toString(){
        StringBuilder message = new StringBuilder();
        for (Word word : words) {
            message.append(word.getWord()).append(" ");
        }
        return message.toString();
    }
}
