package exercises.exercise1;
import java.util.ArrayList;
public class ArrayManager {
    private ArrayList<Number> arrayList;
    public ArrayManager(){
        this.arrayList = new ArrayList<>();
    }

    public void addElement(Number n){
        arrayList.add(n);
    }

    public int[] verifyNumbers(){
        int positiveNumbers = 0;
        int negativeNumbers = 0;

        for (int i = 0; i < arrayList.size(); i++) {
            if(arrayList.get(i).isPositive()){
                positiveNumbers++;
            }else {
                negativeNumbers += arrayList.get(i).getValue();
            }
        }
        return new int[]{positiveNumbers,negativeNumbers};
    }
}
