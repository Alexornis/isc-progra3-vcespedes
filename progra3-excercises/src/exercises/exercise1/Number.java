package exercises.exercise1;

public class Number {
    private int value;

    public Number(int v){
        this.value = v;
    }

    public boolean isPositive(){
        return this.value > 0;
    }

    public int getValue() {
        return value;
    }
}
