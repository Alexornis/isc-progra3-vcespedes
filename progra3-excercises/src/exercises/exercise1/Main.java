package exercises.exercise1;

import java.util.Arrays;

public class Main {
    public static void main(String args[])
    {
        ArrayManager manager = new ArrayManager();
        manager.addElement(new Number(1));
        manager.addElement(new Number(3));
        manager.addElement(new Number(-9));
        manager.addElement(new Number(5));
        manager.addElement(new Number(-18));

        System.out.println(Arrays.toString(manager.verifyNumbers()));
    }
}  