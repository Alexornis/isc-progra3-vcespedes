package exercises.exercise2;

import java.util.ArrayList;

public class EquationsManager {
    public static String sumEquations(Equation eq1, Equation eq2){
        Term[] terms1 = eq1.getTerms();
        Term[] terms2 = eq2.getTerms();

        ArrayList<Term> termsResult = new ArrayList<>();
        for (int i = 0; i < terms1.length; i++) {
            int result = terms1[i].sum(terms2[i].getCoefficient());
            if (terms1[i].getVariable() == null) {
                termsResult.add(new Term(result));
            }else {
                termsResult.add(new Term(result,terms1[i].getVariable().getVar()));
            }

        }
        return String.valueOf(termsResult);
    }
}
