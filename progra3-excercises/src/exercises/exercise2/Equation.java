package exercises.exercise2;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Equation {
    private Term[] terms;

    public Equation(Term... terms){
        this.terms = terms;
    }

    @Override
    public String toString(){
        return Arrays.toString(terms);
    }

    public Term[] getTerms() {
        return terms;
    }
}
