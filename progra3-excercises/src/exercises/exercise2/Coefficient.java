package exercises.exercise2;

public class Coefficient {
    private int value;

    public Coefficient(int v) {
        this.value = v;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
