package exercises.exercise2;

public class Main {
    public static void main(String[] args){
        Equation equation1 = new Equation(
                new Term(5,'x'),
                new Term(10,'y'),
                new Term(7));

        Equation equation2 = new Equation(
                new Term(8,'x'),
                new Term(6,'y'),
                new Term(3));

        System.out.println(equation1);
        System.out.println(equation2);
        System.out.println(EquationsManager.sumEquations(equation1,equation2));
    }
}
