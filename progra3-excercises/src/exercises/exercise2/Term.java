package exercises.exercise2;

public class Term {
    private Coefficient coefficient;
    private Variable variable;

    public Term(int coefficient, char variable){
        this.variable = new Variable(variable);
        this.coefficient = new Coefficient(coefficient);
    }

    public Term(int coefficient){
        this.variable = null;
        this.coefficient = new Coefficient(coefficient);
    }

    public int sum(int number){
        return getCoefficient() + number;
    }

    @Override
    public String toString(){
        if(variable == null) return getCoefficient() + "";
        return getCoefficient() +""+variable.getVar();
    }

    public int getCoefficient() {
        return coefficient.getValue();
    }

    public Variable getVariable() {
        if(variable == null) return null;
        return variable;
    }
}
