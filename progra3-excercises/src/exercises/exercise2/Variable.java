package exercises.exercise2;

public class Variable {

    private char var;

    public Variable( char var){
        this.var = var;
    }

    public char getVar() {
        return var;
    }
}
