package GraphExcercises.Excercise3;

import java.util.ArrayList;

public class GraphManager<T> {

    private boolean isStronglyConnected;
    private ArrayList<T> alreadyUsageVertex;
    private int graphSize ;
    public GraphManager(){
        this.isStronglyConnected = false;
        this.alreadyUsageVertex = new ArrayList<>();
        this.graphSize = 0;
    }

    public boolean verifyStronglyConnected(Graph<T> graph){
        Vertex<T>[] vertices = graph.getVertices();

        graphSize = vertices.length;


        isStronglyConnected = deepFirsSearch(vertices[0],vertices[0].getValue() );

        return isStronglyConnected;
    }


    private boolean deepFirsSearch(Vertex<T> currentVertex,T head){
        if (currentVertex.getConnections().isEmpty()) return false;
        alreadyUsageVertex.add(currentVertex.getValue());

        for (Vertex<T> vertex : currentVertex.getConnections()){
            if(!contains(alreadyUsageVertex,vertex.getValue())) {
                if (vertex.getConnections().isEmpty()){
                    alreadyUsageVertex.remove(currentVertex.getValue());
                    return false;
                }
                if ((alreadyUsageVertex.size() + 1) == graphSize && contains2(vertex.getConnections(),head))
                    return true;
                return deepFirsSearch( vertex,head);
            }
        }

        alreadyUsageVertex.remove(currentVertex.getValue());
        return false;
    }

    private boolean contains(ArrayList<T> list , T value){
        for (T vertex : list){
            if (vertex.equals(value)) return true;
        }
        return false;
    }

    private boolean contains2(ArrayList<Vertex<T>> list , T value){
        for (Vertex<T> vertex : list){
            if (vertex.getValue().equals(value)) return true;
        }
        return false;
    }
}
