package GraphExcercises.Excercise3;

public class Main {

    public static void main(String[] args){

        Graph<Character> graph = new Graph(
                new Vertex<>('A'),new Vertex<>('B'),new Vertex<>('C'),
                new Vertex<>('D'),new Vertex<>('E'));

        graph.makeDirectionalEdge('A','D');graph.makeDirectionalEdge('B','A');
        graph.makeDirectionalEdge('C','B');graph.makeDirectionalEdge('C','E');
        graph.makeDirectionalEdge('E','A');graph.makeDirectionalEdge('E','C');
        graph.makeDirectionalEdge('D','E');graph.makeDirectionalEdge('D','C');

        System.out.println(graph);

        GraphManager<Character> manager = new GraphManager<>();
        System.out.println(manager.verifyStronglyConnected(graph));

       /* Graph<Integer> graph2 = new Graph(
                new Vertex<>(1),new Vertex<>(2),new Vertex<>(3),
                new Vertex<>(4));

        graph2.makeDirectionalEdge(2,1);graph2.makeDirectionalEdge(3,2);
        graph2.makeDirectionalEdge(3,4);

        System.out.println(graph2);
        GraphManager<Integer> manager = new GraphManager<>();
        System.out.println(manager.verifyStronglyConnected(graph2));*/
    }
}
