package GraphExcercises.Excercise2;

public class Main {

    public static void main(String[] args) {
        Graph<Integer> graph = new Graph(
                new Vertex<>(1),new Vertex<>(2),new Vertex<>(3),
                new Vertex<>(4),new Vertex<>(5),new Vertex<>(6));

        graph.makeUnDirectionalEdge(1,3);graph.makeUnDirectionalEdge(1,2);
        graph.makeUnDirectionalEdge(3,4);graph.makeUnDirectionalEdge(2,4);
        graph.makeUnDirectionalEdge(3,5);graph.makeUnDirectionalEdge(4,5);
        graph.makeUnDirectionalEdge(4,6);

        GraphManager<Integer> manager = new GraphManager<>();

        manager.searchCycles(graph);

        /*

        Graph<Integer> graph2 = new Graph(
                new Vertex<>(1),new Vertex<>(2),new Vertex<>(3),
                new Vertex<>(4));

        graph2.makeUnDirectionalEdge(1,2);graph2.makeUnDirectionalEdge(1,3);

        GraphManager<Integer> manager = new GraphManager<>();

        manager.searchCycles(graph2);
         */
    }
}
