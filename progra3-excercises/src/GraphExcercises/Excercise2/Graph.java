package GraphExcercises.Excercise2;

public class Graph<T> {
    private Vertex<T>[] vertices;
    public Graph(Vertex<T>... vertices){
        this.vertices = vertices;
    }

    public void makeUnDirectionalEdge(T value1,T value2){
        Vertex<T> firstUser = getVertex(value1);
        Vertex<T> secondUser = getVertex(value2);

        firstUser.addRelationship(secondUser);
        secondUser.addRelationship(firstUser);
    }

    public void makeDirectionalEdge(T value1,T value2){
        Vertex<T> firstUser = getVertex(value1);
        Vertex<T> secondUser = getVertex(value2);

        firstUser.addRelationship(secondUser);
    }

    public Vertex<T> getVertex(T value){
        Vertex<T> v = null;
        for (Vertex<T> vertex : vertices){
            if (vertex.getValue().equals(value)) v = vertex;
        }
        return v;
    }

    @Override
    public String toString(){
        String message = "";
        for (Vertex<T> vertex : vertices){
            message = message + " -" + vertex.getValue() + vertex + "\n";
        }
        return message;
    }

    public Vertex<T>[] getVertices() {
        return vertices;
    }
}
