package GraphExcercises.Excercise2;

import java.util.ArrayList;

public class GraphManager<T> {
    private ArrayList<Vertex<T>> disapprovedVertices;
    private ArrayList<Vertex<T>> possibleInitCycle;
    ArrayList<ArrayList<T>> cycles ;

    public GraphManager(){
        this.disapprovedVertices = new ArrayList<>();
        this.possibleInitCycle = new ArrayList<>();
        this.cycles = new ArrayList<>();
    }

    public void searchCycles(Graph<T> graph){
        discardVertices(graph.getVertices()) ;
        for (Vertex<T> vertex : possibleInitCycle){
            if(!isDiscarded(vertex)){
                ArrayList<T> alreadyUsageVertex = new ArrayList<>();
                alreadyUsageVertex.add(vertex.getValue());
                deepFirsSearch(alreadyUsageVertex,vertex.getValue(),vertex.getConnections());
            }
        }
        clearCycles();
        deleteRepeats();
        showCycles();
    }

    private void deepFirsSearch(ArrayList<T> alreadyUsageVertex , T head, ArrayList<Vertex<T>> edges){
        for (Vertex<T> vertex : edges){
            if(!isDiscarded(vertex) ){
                if(vertex.getValue().equals(head)) {
                    saveCycle(alreadyUsageVertex);
                }
                else if(!contains(alreadyUsageVertex,vertex.getValue())){
                    ArrayList<Vertex<T>> nextEdges = vertex.getConnections();
                    ArrayList<T> newList = new ArrayList<>();
                    newList.addAll(alreadyUsageVertex);
                    newList.add(vertex.getValue());
                    deepFirsSearch(newList,head,nextEdges);
                }

            }
        }
    }

    private boolean contains(ArrayList<T> list , T value){
        for (T vertex : list){
            if (vertex.equals(value)) return true;
        }
        return false;
    }

    private boolean isDiscarded(Vertex<T> vertex){
        for (Vertex<T> vertexDiscarded : disapprovedVertices){
            if (vertexDiscarded.equals(vertex)) return true;
        }
        return false;
    }

    public void discardVertices(Vertex<T>[] vertices ){
        possibleInitCycle.clear();
        disapprovedVertices.clear();
        cycles.clear();
        for(Vertex<T> vertex : vertices){
            if(vertex.getConnections().size() > 1) possibleInitCycle.add(vertex);
            else disapprovedVertices.add(vertex);
        }

    }

    private void saveCycle(ArrayList<T> cycleNew){
        cycles.add(cycleNew);
    }

    private void clearCycles(){
        for (int i = 0; i < cycles.size(); i++) {
            if (cycles.get(i).size() < 3) cycles.remove(i);
        }
    }

    private void deleteRepeats(){
        ArrayList<Integer> indexRepeat = new ArrayList<>();
        ArrayList<ArrayList<T>> saveList = new ArrayList<>();
        for (int i = 0; i < cycles.size(); i++) {
            if (!indexRepeat.contains(i)){
                ArrayList<T> cycle = cycles.get(i);
                saveList.add(cycle);
                for (int j = 0; j < cycles.size(); j++) {
                    if(compareCycles(cycle,cycles.get(j))) indexRepeat.add(j);
                }
            }
        }
        cycles.clear();
        cycles.addAll(saveList);
    }

    private boolean compareCycles(ArrayList<T> list1,ArrayList<T> list2){
        if (list1.size() != list2.size()) return false;
        boolean validation;
        for (T value : list1){
            validation = false;
            for (T value2 : list2){
                if (value == value2) {
                    validation = true;
                    break;
                }
            }
            if (!validation) return false;
        }
        return true;
    }

    private void showCycles(){
        if (cycles.size() == 0) {
            System.out.println(" NO CYCLES ");
        }
        else {
            System.out.println("- CYCLES : " + cycles.size());
            for (ArrayList<T> cycle : cycles){
                StringBuilder message = new StringBuilder("Cycle : ");
                for( T value : cycle) {
                    message.append(value).append(" -> ");
                }
                System.out.println(message.toString() + cycle.get(0));
            }

        }
    }
}
