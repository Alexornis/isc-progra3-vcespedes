package GraphExcercises.Excercise1;

public class GraphManager<T> {

    public boolean compareGraphs(Graph<T> graph1, Graph<T> graph2){
        Vertex<T>[] vertices = graph1.getVertices();
        Graph<T> graph = graph2;
        if (graph1.getVertices().length != graph2.getVertices().length) return false;
        for (Vertex<T> vertex1 : vertices){
            boolean validation = false;
            for (Vertex<T> vertex2 : graph.getVertices()){
                if (vertex1.equals(vertex2)) validation = true;
            }
            if (!validation) return false;
        }
        return true;

    }
}
