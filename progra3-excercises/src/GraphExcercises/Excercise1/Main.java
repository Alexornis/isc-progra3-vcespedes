package GraphExcercises.Excercise1;

public class Main {

    public static void main(String[] args){
    //    CASE 1
        Graph<Integer> graph = new Graph(
                new Vertex<>(1),new Vertex<>(2),new Vertex<>(3),
                new Vertex<>(4),new Vertex<>(5),new Vertex<>(6));

        graph.makeUnDirectionalEdge(1,3);graph.makeUnDirectionalEdge(1,2);
        graph.makeUnDirectionalEdge(2,4);graph.makeUnDirectionalEdge(3,4);
        graph.makeUnDirectionalEdge(3,5);graph.makeUnDirectionalEdge(4,5);
        graph.makeUnDirectionalEdge(4,6);

        System.out.println("GRAPH 1:");
        System.out.println(graph);

        Graph<Integer> graph2 = new Graph(
                new Vertex<>(1),new Vertex<>(2),new Vertex<>(3),
                new Vertex<>(4),new Vertex<>(5),new Vertex<>(6));

        graph2.makeUnDirectionalEdge(1,3);graph2.makeUnDirectionalEdge(1,2);
        graph2.makeUnDirectionalEdge(2,4);graph2.makeUnDirectionalEdge(3,4);
        graph2.makeUnDirectionalEdge(3,5);graph2.makeUnDirectionalEdge(4,5);
        graph2.makeUnDirectionalEdge(4,6);

        System.out.println("GRAPH 2:");
        System.out.println(graph2);
        GraphManager<Integer> manager = new GraphManager<>();
        System.out.println("GRAPH 1 IS EQUALS GRAPH 2 :");
        System.out.println(manager.compareGraphs(graph,graph2));

        // CASE2
/*
        Graph<Integer> graph = new Graph(
                new Vertex<>(1),new Vertex<>(2),new Vertex<>(3),
                new Vertex<>(4),new Vertex<>(5));

        graph.makeDirectionalEdge(2,1);
        graph.makeDirectionalEdge(3,2);
        graph.makeDirectionalEdge(5,2);
        graph.makeDirectionalEdge(3,5);
        graph.makeDirectionalEdge(4,3);

        System.out.println("GRAPH 1:");
        System.out.println(graph);

        Graph<Integer> graph2 = new Graph(
                new Vertex<>(1),new Vertex<>(2),new Vertex<>(3),
                new Vertex<>(4),new Vertex<>(5));

        graph2.makeDirectionalEdge(2,1);
        graph2.makeDirectionalEdge(3,2);
        graph2.makeDirectionalEdge(5,2);
        graph2.makeDirectionalEdge(5,4);
        graph2.makeDirectionalEdge(3,5);

        System.out.println("GRAPH 2:");
        System.out.println(graph2);
        GraphManager<Integer> manager = new GraphManager<>();
        System.out.println(manager.compareGraphs(graph,graph2));

 */
    }
}
