package ExcercisesTree2.Excercises2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

public class BinaryTree<T extends Comparable<T>> implements IBinaryTree<T> {
    private INode<T> root;
    private int numberOfNodes;

    public BinaryTree() {
        this.root = null;
        this.numberOfNodes = 0;
    }


    @Override
    public void print() {

    }

    @Override
    public int getWeight() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public void insert(T newData, int key) {
        numberOfNodes ++;
        this.root = insertRecursive(root, newData,key);

    }

    private INode<T> insertRecursive(INode<T> root, T newData, int key) {
        if (root == null) {
            root = new Node<>(newData,key);
            return root;//
        }
        else if (root.getKey() > key) {
            root.setLeftNode(insertRecursive(root.getLeftNode(), newData, key));
        } else {
            if (root.getKey() < key) {
                root.setRightNode(insertRecursive(root.getRightNode(), newData, key));
            }
        }
        return root;
    }

    public String traverseIterativeBFS(){
        ArrayList<INode<T>> finalList =  new ArrayList<>();
        ArrayList<INode<T>> currentLevelNodes =  new ArrayList<>();
        ArrayList<INode<T>> nextLevelNodes = new ArrayList<>();
        if(root != null){
            nextLevelNodes.add(root);
            while (!nextLevelNodes.isEmpty()){
                finalList.addAll(nextLevelNodes);
                currentLevelNodes.addAll(nextLevelNodes);
                nextLevelNodes.clear();
                for (INode<T> node : currentLevelNodes){
                    if (node.hasLeft()) nextLevelNodes.add(node.getLeftNode());
                    if (node.hasRight()) nextLevelNodes.add(node.getRightNode());
                }
                currentLevelNodes.clear();
            }
        }
        return toStr(finalList);
    }

    private String toStr(ArrayList<INode<T>> nodes){
        StringBuilder str = new StringBuilder();
        for (INode<T> node : nodes){
            str.append(" ").append(node.getData());
        }
        return str.toString();
    }


    //Traversal Tree
    public void printPreOrderRecursiveDFS() {
        printPreOrderRecursiveDFS(root);
    }


    @Override
    public void printInOrderRecursiveDFS() {
        printInOrderRecursiveDFS(root);
    }

    public void printInOrderRecursiveDFS(INode<T> node) {
        if (node != null) {
            printInOrderRecursiveDFS(node.getLeftNode());
            System.out.print(node.getData() + " - ");
            printInOrderRecursiveDFS(node.getRightNode());
        }
    }

    @Override
    public void printInOrderIterativeDFS() {
        Stack<INode<T>> stack =  new Stack<>();
        INode<T> currentNode = this.root;

        while (currentNode != null || !stack.isEmpty()) {
            while (currentNode != null) {
                stack.push(currentNode);
                currentNode = currentNode.getLeftNode();
            }

            INode<T> n = stack.pop();
            System.out.print(n.getData() + " - ");
            currentNode = n.getRightNode();
        }
    }

    @Override
    public Iterator<INode<T>> iterator() {
        return null;
    }

    @Override
    public int getNumberOfNodes() {
        return numberOfNodes;
    }

    @Override
    public int height() {
        return 0;
    }

    @Override
    public void delete(T node) {

    }

    private void printPreOrderRecursiveDFS(INode<T> root) {
        if (root == null) {
            return;
        }
        System.out.print(root.getData() + " ");
        printPreOrderRecursiveDFS(root.getLeftNode());
        printPreOrderRecursiveDFS(root.getRightNode());
    }

    @Override
    public void printBFS() {

    }

    @Override
    public void printDFS() {

    }

    @Override
    public void printPreOrderIterativeDFS() {

    }

    @Override
    public INode<T> getRoot() {
        return root;
    }
}