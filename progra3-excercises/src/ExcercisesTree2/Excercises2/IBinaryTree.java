package ExcercisesTree2.Excercises2;

import java.util.Iterator;

public interface IBinaryTree<T> {
    void insert(T node,int key);
    void delete(T node);

    void print();

    int getWeight();
    int getHeight();

    void printBFS(); // preorder o postorder
    void printDFS(); // preorder o postorder

    void printPreOrderIterativeDFS();
    void printPreOrderRecursiveDFS();

    void printInOrderRecursiveDFS();
    void printInOrderIterativeDFS();

    Iterator<INode<T>> iterator();

    int getNumberOfNodes();
    int height();

    INode<T> getRoot();
}
