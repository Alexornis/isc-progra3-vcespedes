package ExcercisesTree2.Excercises2;

public class Main {
    public static void main(String[] args) {

        BinaryTree<String> binaryTree = new BinaryTree<>();
        binaryTree.insert("Tres",40);
        binaryTree.insert("Tristes",20);
        binaryTree.insert("Tigres",60);
        binaryTree.insert("Comian",10);
        binaryTree.insert("Trigo",30);

        System.out.println(binaryTree.traverseIterativeBFS());
        //the appropriate print type is the BFS

    }
}
