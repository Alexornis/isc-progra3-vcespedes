package ExcercisesTree2.Excercises2;

public interface INode<T> {
    T getData();
    void setData(T d);

    INode<T> getLeftNode();
    void setLeftNode(INode<T> leftNode);

    INode<T> getRightNode();
    void setRightNode(INode<T> rightNode);

    int getLevel();

    boolean hasLeft();

    boolean hasRight();

    int getKey();
    boolean equalsNode(INode<T> node);
}
