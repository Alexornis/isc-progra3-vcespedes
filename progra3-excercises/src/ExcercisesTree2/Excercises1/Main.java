package ExcercisesTree2.Excercises1;

public class Main {
    public static void main(String[] args) {
        BinarySearchTree<Integer> binarySearchTree = new BinarySearchTree<>();

        binarySearchTree.insert(45);
        binarySearchTree.insert(10);
        binarySearchTree.insert(90);
        binarySearchTree.insert(7);
        binarySearchTree.insert(12);
        binarySearchTree.insert(50);

        System.out.println(binarySearchTree.traverseIterativeBFS());

        binarySearchTree.delete(12);

        System.out.println(binarySearchTree.traverseIterativeBFS());

        System.out.println(" Height : " + binarySearchTree.getHeight());

    }
}
