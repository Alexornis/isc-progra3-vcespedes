package ExcercisesTree2.Excercises1;


import java.util.Iterator;

public interface IBinarySearchTree<T> {
    void insert(T node);
    boolean delete(T node);

    //    TreeNode<T> findNode(TreeNode<T> node);
    boolean search(T node);

    void print();

    int getWeight();
    int getHeight();

    void printBFS(); // preorder o postorder
    void printDFS(); // preorder o postorder

    void printPreOrderIterativeDFS();
    void printPreOrderRecursiveDFS();

    void printInOrderRecursiveDFS();
    void printInOrderIterativeDFS();

    Iterator<INode<T>> iterator();

    int getNumberOfNodes();
    int height();
}
