package ExcercisesTree2.Excercises1;


public class Node<T> implements INode<T> {
    //instance variable of Node class
    private T data;
    private int height;
    private INode<T> left;
    private INode<T> right;

    //constructor
    public Node(T data) {
        this.data = data;
        this.left = null;
        this.right = null;
        this.height = 0;
    }

    @Override
    public boolean hasLeft(){
        return left!=null;
    }

    @Override
    public boolean hasRight(){
        return right!=null;
    }

    @Override
    public void setHeight(int height) {
        this.height = height;

    }

    public T getData() {
        return data;
    }

    @Override
    public void setData(T d) {
        this.data = d;
    }

    @Override
    public INode<T> getLeftNode() {
        return this.left;
    }

    @Override
    public INode<T> getRightNode() {
        return this.right;
    }

    @Override
    public int getLevel() {
        return 0;
    }

    public void setLeftNode(INode<T> left) {
        this.left = left;
    }

    public void setRightNode(INode<T> right) {
        this.right = right;
    }

}