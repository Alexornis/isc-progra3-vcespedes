package ExcercisesTree2.Excercises1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;

public class BinarySearchTree<T extends Comparable<T>> implements IBinarySearchTree<T> {
    private int height;
    // instance variable
    private INode<T> root;

    // constructor for initialise the root to null BYDEFAULT
    public BinarySearchTree() {
        this.root = null;
        this.height = 0;
    }

    // insert method to insert the new Date

    @Override
    public int getHeight() {
        return height;
        // para este metodo actualize el heght cada que se inserta un nodo
    }


    @Override
    public boolean delete(T node) {
        if(node.equals(root.getData())) {
            this.root = null;
            return true;
        }
        else{
            ArrayList<INode<T>> currentLevelNodes =  new ArrayList<>();
            ArrayList<INode<T>> nextLevelNodes = new ArrayList<>();
            if(root != null){
                nextLevelNodes.add(root);
                while (!nextLevelNodes.isEmpty()){
                    currentLevelNodes.addAll(nextLevelNodes);
                    nextLevelNodes.clear();
                    for (INode<T> nod : currentLevelNodes){
                        if (nod.hasLeft()) {
                            if(nod.getLeftNode().getData().equals(node)){
                                nod.setLeftNode(null);
                                return true;
                            }
                            else nextLevelNodes.add(nod.getLeftNode());
                        }
                        if (nod.hasRight()) {
                            if(nod.getRightNode().getData().equals(node)){
                                nod.setRightNode(null);
                                return true;
                            }
                            else nextLevelNodes.add(nod.getRightNode());
                        }
                    }
                    currentLevelNodes.clear();
                }
            }
        }
        return false;
    }



    @Override
    public void insert(T newData) {
        this.root = insertRecursive(root, newData,1);
    }

    private INode<T> insertRecursive(INode<T> root, T newData, int level) {
        if (root == null) {
            if(this.height < level) this.height = level;
            root = new Node<>(newData);
            root.setHeight(height);
            return root;//40
        }
        else if (newData.compareTo(root.getData()) < 0) {
            root.setLeftNode(insertRecursive(root.getLeftNode(), newData, level+1));
        } else {
            if (newData.compareTo(root.getData()) > 0) {
                root.setRightNode(insertRecursive(root.getRightNode(), newData, level+1));
            }
        }

        return root;
    }

    @Override
    public boolean search(T node) {
        return search(root, new Node(node));
    }

    private boolean search(INode<T> current, INode<T> node){
        if (current == null){
            return false;
        }
        else {
            if (current.getData().compareTo(node.getData()) == 0) return true;

            else if (node.getData().compareTo(current.getData()) < 0) {
                return search(current.getLeftNode(), node);
            }
            else if (node.getData().compareTo(current.getData()) > 0){
                return search(current.getRightNode(), node);
            }

            return false;
        }
    }

    @Override
    public void print() {

    }

    @Override
    public int getWeight() {
        return 0;
    }



    @Override
    public void printBFS() {

    }

    @Override
    public void printDFS() {

    }

    @Override
    public void printPreOrderIterativeDFS() {

    }

    @Override
    public void printPreOrderRecursiveDFS() {

    }

    @Override
    public void printInOrderRecursiveDFS() {

    }

    @Override
    public void printInOrderIterativeDFS() {

    }

    @Override
    public Iterator<INode<T>> iterator() {
        return null;
    }

    @Override
    public int getNumberOfNodes() {
        return 0;
    }

    @Override
    public int height() {
        return 0;
    }

    public String traverseIterativeBFS(){
        ArrayList<INode<T>> finalList =  new ArrayList<>();
        ArrayList<INode<T>> currentLevelNodes =  new ArrayList<>();
        ArrayList<INode<T>> nextLevelNodes = new ArrayList<>();
        if(root != null){
            nextLevelNodes.add(root);
            while (!nextLevelNodes.isEmpty()){
                finalList.addAll(nextLevelNodes);
                currentLevelNodes.addAll(nextLevelNodes);
                nextLevelNodes.clear();
                for (INode<T> node : currentLevelNodes){
                    if (node.hasLeft()) nextLevelNodes.add(node.getLeftNode());
                    if (node.hasRight()) nextLevelNodes.add(node.getRightNode());
                }
                currentLevelNodes.clear();
            }
        }
        return toStr(finalList);
    }

    private String toStr(ArrayList<INode<T>> nodes){
        StringBuilder str = new StringBuilder();
        for (INode<T> node : nodes){
            str.append(" - ").append(node.getData());
        }
        return str.toString();
    }

}